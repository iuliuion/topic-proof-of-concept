package it.ts.dotcom.topic.proofOfConcept.repository;

import it.ts.dotcom.topic.proofOfConcept.entity.ComponentWorkingHours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.Optional;

public interface ComponentWorkingHoursRepository extends JpaRepository<ComponentWorkingHours, Long> {

    @Query(
            value = "WITH last_job_completion AS    " +
                    "(   " +
                    "   SELECT   " +
                    "      COALESCE ((   " +
                    "      SELECT   " +
                    "         tcwh.working_hours    " +
                    "      FROM   " +
                    "         poc.t_components_working_hours tcwh    " +
                    "      WHERE   " +
                    "         tcwh.component_id = :componentId    " +
                    "         AND tcwh.creation_time < :initialDate    " +
                    "      ORDER BY   " +
                    "         tcwh.creation_time DESC LIMIT 1 ), 0) AS working_hours    " +
                    ")   " +
                    "SELECT   " +
                    "   tcwh1.*    " +
                    "FROM   " +
                    "   poc.t_components_working_hours tcwh1,   " +
                    "   last_job_completion    " +
                    "WHERE   " +
                    "   tcwh1.component_id = :componentId    " +
                    "   AND tcwh1.creation_time > :initialDate    " +
                    "   AND tcwh1.working_hours >=    " +
                    "   (   " +
                    "      last_job_completion.working_hours + :workingHoursInterval    " +
                    "   )   " +
                    "ORDER BY   " +
                    "   tcwh1.creation_time ASC LIMIT 1",
            nativeQuery = true)
    Optional<ComponentWorkingHours> getFirstAfterWorkingHoursInterval(Long componentId, LocalDateTime initialDate, Long workingHoursInterval);

    Optional<ComponentWorkingHours> getFirstByComponentIdOrderByCreationTimeDesc(Long componentId);

}

package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PortArrivalDto {

    private Long id;
    private String destination;
    private LocalDateTime arrivalDate;
    private LocalDateTime departureDate;

}

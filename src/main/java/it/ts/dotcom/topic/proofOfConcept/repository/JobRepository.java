package it.ts.dotcom.topic.proofOfConcept.repository;

import it.ts.dotcom.topic.proofOfConcept.entity.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface JobRepository extends JpaRepository<Job, Long> {

    List<Job> findByCompletionTimeIsNull();

    @Query(
            value = "select" +
                    "   t_last_completed_jobs.* " +
                    "from" +
                    "   poc.t_schedule_rules tsr " +
                    "   join" +
                    "      lateral ( " +
                    "      select" +
                    "         * " +
                    "      from" +
                    "         poc.t_jobs tj " +
                    "      where" +
                    "         tj.schedule_rule_id = tsr.id " +
                    "         and tj.completion_time is not null " +
                    "         and tsr.start_date <= current_timestamp " +
                    "         and tsr.end_date is null " +
                    "      order by" +
                    "         tj.completion_time desc limit 1 ) t_last_completed_jobs " +
                    "         on true " +
                    "      order by" +
                    "         t_last_completed_jobs.schedule_rule_id",
            nativeQuery = true)
    List<Job> findLastCompletedRuleJobs();

}

package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PortArrivalDtoSend {

    private String destination;
    private LocalDateTime arrivalDate;
    private LocalDateTime departureDate;

}

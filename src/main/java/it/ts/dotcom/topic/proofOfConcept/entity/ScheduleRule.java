package it.ts.dotcom.topic.proofOfConcept.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_schedule_rules", schema = "poc")
public class ScheduleRule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "component_id")
    private Component component;
    @Column(name = "rule_type")
    private String ruleType;
    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;
    @Column(name = "frequency_type")
    private String frequencyType;
    @Column(name = "days_frequency")
    private Long daysFrequency;
    @Column(name = "working_hours_frequency")
    private Long workingHoursFrequency;
    @Column(name = "description")
    private String description;
}

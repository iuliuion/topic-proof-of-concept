package it.ts.dotcom.topic.proofOfConcept.dto;

import it.ts.dotcom.topic.proofOfConcept.entity.Component;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ScheduleRuleDto {

    private Long id;
    private String name;
    private Component component;
    private String ruleType;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String frequencyType;
    private Long daysFrequency;
    private Long workingHoursFrequency;
    private String description;

}

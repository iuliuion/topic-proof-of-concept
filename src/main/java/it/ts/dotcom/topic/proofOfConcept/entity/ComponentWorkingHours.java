package it.ts.dotcom.topic.proofOfConcept.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_components_working_hours", schema = "poc")
public class ComponentWorkingHours {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "component_id")
    private Component component;
    @Column(name = "creation_time")
    private LocalDateTime creationTime;
    @Column(name = "working_hours")
    private Long workingHours;
    @Column(name = "comments")
    private String comments;

}
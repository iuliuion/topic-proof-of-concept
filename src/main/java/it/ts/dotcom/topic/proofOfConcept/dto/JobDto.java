package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class JobDto {

    private Long id;
    private String name;
    private String type;
    private ComponentDto component;
    private ScheduleRuleDto scheduleRule;
    private LocalDateTime scheduledTime;
    private LocalDateTime completionTime;
    private List<WorkedHoursDto> workedHours;
    private String status;
    private Boolean crew;
    private Boolean outsourced;
    private Boolean classAttendance;
    private String comments;
    private String attachmentUrl;

}

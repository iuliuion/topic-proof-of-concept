package it.ts.dotcom.topic.proofOfConcept.repository;

import it.ts.dotcom.topic.proofOfConcept.entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogRepository extends JpaRepository<Log, Long> {
}

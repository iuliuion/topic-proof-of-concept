package it.ts.dotcom.topic.proofOfConcept.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_logs", schema = "poc")
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "crew_member")
    private String crewMember;
    @ManyToOne
    @JoinColumn(name = "job_id")
    private Job job;
    @Column(name = "log_time")
    private LocalDateTime logTime;
    @Column(name = "event_logged")
    private String eventLogged;
    @Column(name = "attachment_url")
    private String attachmentUrl;

}

package it.ts.dotcom.topic.proofOfConcept.service;

import com.github.dozermapper.core.Mapper;
import it.ts.dotcom.topic.proofOfConcept.dto.JobDto;
import it.ts.dotcom.topic.proofOfConcept.dto.JobDtoSend;
import it.ts.dotcom.topic.proofOfConcept.dto.WorkedHoursDto;
import it.ts.dotcom.topic.proofOfConcept.entity.*;
import it.ts.dotcom.topic.proofOfConcept.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static it.ts.dotcom.topic.proofOfConcept.util.Vars.*;

@Service
@Transactional
@Slf4j
public class JobService {

    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private ScheduleRuleRepository scheduleRuleRepository;
    @Autowired
    private PortArrivalRepository portArrivalRepository;
    @Autowired
    private ComponentWorkingHoursRepository componentWorkingHoursRepository;
    @Autowired
    private ComponentRepository componentRepository;
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private Mapper beanMapper;

    public List<JobDto> getAll() {
        return jobRepository.findAll().stream().map(job -> beanMapper.map(job, JobDto.class)).collect(Collectors.toList());
    }

    public List<JobDto> getAllLikeExample(JobDtoSend jobDtoSend) {
        Example<Job> jobExample = Example.of(beanMapper.map(jobDtoSend, Job.class));
        if (jobExample.getProbe().getScheduleRule().getId() == null) {
            jobExample.getProbe().setScheduleRule(null);
        }
        if (jobExample.getProbe().getComponent().getId() == null) {
            jobExample.getProbe().setComponent(null);
        }
        return jobRepository.findAll(jobExample).stream().map(job
                -> beanMapper.map(job, JobDto.class)).collect(Collectors.toList());
    }

    public JobDto findById(Long id) {
        return beanMapper.map(jobRepository.findById(id).orElseThrow(()
                -> new RuntimeException(JOB_NOT_FOUND_MESSAGE + id)), JobDto.class);
    }

    public void deleteById(Long id) {
        jobRepository.findById(id).orElseThrow(() -> new RuntimeException(JOB_NOT_FOUND_MESSAGE + id));
        jobRepository.deleteById(id);
    }

    public JobDto create(JobDtoSend jobDtoSend) {
        validateJob(jobDtoSend);
        Job job = beanMapper.map(jobDtoSend, Job.class);
        setJobScheduleRuleFromDto(jobDtoSend, job);
        return beanMapper.map(jobRepository.save(job), JobDto.class);
    }

    public JobDto update(Long id, JobDtoSend jobDtoSend) {
        validateJob(jobDtoSend);
        String oldJobStatus = jobRepository.findById(id).orElseThrow(()
                -> new RuntimeException(JOB_NOT_FOUND_MESSAGE + id)).getStatus();
        Job transitoryJob = new Job();
        transitoryJob.setId(id);
        beanMapper.map(jobDtoSend, transitoryJob);
        if ((jobDtoSend.getStatus() != null) && (oldJobStatus != jobDtoSend.getStatus())
                && (jobDtoSend.getStatus().equalsIgnoreCase(JobStatusCategories.DONE.name())
                || jobDtoSend.getStatus().equalsIgnoreCase(JobStatusCategories.CANCELLED.name())))
            transitoryJob.setCompletionTime(LocalDateTime.now());
        setJobScheduleRuleFromDto(jobDtoSend, transitoryJob);
        Job job = jobRepository.save(transitoryJob);
        if (jobDtoSend.getStatus() != null && oldJobStatus != jobDtoSend.getStatus()) {
            Log jobLog = new Log();
            jobLog.setJob(job);
            jobLog.setCrewMember("SYSTEM");
            jobLog.setLogTime(LocalDateTime.now());
            jobLog.setEventLogged(job.getStatus());
            logRepository.save(jobLog);
        }
        JobDto result = beanMapper.map(job, JobDto.class);
        return result;
    }

    public List<JobDto> getAllActiveJobs() {
        ArrayList<JobDto> allActiveJobsDtos = new ArrayList<>();

        ArrayList<ScheduleRule> activeRules = (ArrayList<ScheduleRule>) scheduleRuleRepository.findAllActiveRules();
        ArrayList<Job> savedActiveJobs = (ArrayList<Job>) jobRepository.findByCompletionTimeIsNull();
        ArrayList<Job> lastCompletedRuleJobs = (ArrayList<Job>) jobRepository.findLastCompletedRuleJobs();

        for (Job activeJob : savedActiveJobs) {
            if (activeJob.getScheduleRule() != null) {
                activeRules.removeIf(scheduleRule -> scheduleRule.getId().equals(activeJob.getScheduleRule().getId()));
            }
            JobDto activeJobDto = new JobDto();
            beanMapper.map(activeJob, activeJobDto);
            allActiveJobsDtos.add(activeJobDto);
        }
        for (Job lastCompletedRuleJob : lastCompletedRuleJobs) {

            activeRules.removeIf(scheduleRule -> scheduleRule.getId().equals(lastCompletedRuleJob.getScheduleRule().getId()));
            generateJobDto(lastCompletedRuleJob.getScheduleRule(), lastCompletedRuleJob.getCompletionTime())
                    .ifPresent(jobDto -> allActiveJobsDtos.add(jobDto));
        }
        for (ScheduleRule remainingActiveRule : activeRules) {
            generateJobDto(remainingActiveRule).ifPresent(jobDto -> allActiveJobsDtos.add(jobDto));
        }
        for (JobDto activeJobDto : allActiveJobsDtos) {
            if (activeJobDto.getScheduledTime() != null
                    && activeJobDto.getStatus() == null
                    && activeJobDto.getScheduledTime().isBefore(LocalDateTime.now().truncatedTo(ChronoUnit.DAYS)))

                activeJobDto.setStatus(JobStatusCategories.DELAYED.name());
        }

        return allActiveJobsDtos;
    }

    private Optional<JobDto> generateJobDto(ScheduleRule scheduleRule) {
        return this.generateJobDto(scheduleRule, scheduleRule.getStartDate());
    }

    private Optional<JobDto> generateJobDto(ScheduleRule scheduleRule, LocalDateTime startTime) {
        Job generatedJob = new Job();
        ScheduleRuleFrequencyTypes frequencyType = ScheduleRuleFrequencyTypes
                .valueOf(scheduleRule.getFrequencyType());

        switch (frequencyType) {
            case EACH_PORT_DOCK_STOP: {
                Optional<PortArrival> nextPortArrival = portArrivalRepository.getNextPortArrival();
                if (nextPortArrival.isPresent() && nextPortArrival.get().getArrivalDate().isAfter(startTime)
                        && LocalDateTime.now().isAfter(nextPortArrival.get().getArrivalDate().minusDays(3))) {
                    if (nextPortArrival.get().getDepartureDate() != null)
                        generatedJob.setScheduledTime(nextPortArrival.get().getDepartureDate().truncatedTo(ChronoUnit.DAYS));
                } else return Optional.empty();
                break;
            }
            case EACH_WORKING_HOURS_INTERVAL: {
                Optional<ComponentWorkingHours> componentWorkingHoursAfterInterval = componentWorkingHoursRepository
                        .getFirstAfterWorkingHoursInterval(scheduleRule.getComponent().getId(), startTime, scheduleRule.getWorkingHoursFrequency());
                if (componentWorkingHoursAfterInterval.isPresent()) {
                    generatedJob.setScheduledTime(componentWorkingHoursAfterInterval.get().getCreationTime().truncatedTo(ChronoUnit.DAYS).plusDays(1));
                } else return Optional.empty();
                break;
            }
            case EACH_DAYS_INTERVAL: {
                generatedJob.setScheduledTime(startTime.truncatedTo(ChronoUnit.DAYS).plusDays(scheduleRule.getDaysFrequency()));
                break;
            }
            case EACH_DAYS_OR_WORKING_HOURS_INTERVAL: {
                Optional<ComponentWorkingHours> componentWorkingHoursAfterInterval = componentWorkingHoursRepository
                        .getFirstAfterWorkingHoursInterval(scheduleRule.getComponent().getId(), startTime, scheduleRule.getWorkingHoursFrequency());
                if (componentWorkingHoursAfterInterval.isPresent()) {
                    generatedJob.setScheduledTime(componentWorkingHoursAfterInterval.get().getCreationTime().truncatedTo(ChronoUnit.DAYS).plusDays(1));
                } else
                    generatedJob.setScheduledTime(startTime.truncatedTo(ChronoUnit.DAYS).plusDays(scheduleRule.getDaysFrequency()));
                break;
            }
        }

        generatedJob.setName(scheduleRule.getName() + "_job");
        generatedJob.setType(scheduleRule.getRuleType());
        generatedJob.setComponent(scheduleRule.getComponent());
        generatedJob.setScheduleRule(scheduleRule);
        return Optional.ofNullable(beanMapper.map(generatedJob, JobDto.class));
    }

    private void setJobScheduleRuleFromDto(JobDtoSend jobDtoSend, Job targetJob) {
        if (jobDtoSend.getScheduleRuleId() != null) {
            ScheduleRule referencedScheduleRule = scheduleRuleRepository.findById(targetJob.getScheduleRule().getId()).orElseThrow(()
                    -> new RuntimeException(SCHEDULE_RULE_NOT_FOUND_MESSAGE + targetJob.getScheduleRule().getId()));
            targetJob.setScheduleRule(referencedScheduleRule);
            targetJob.setType(referencedScheduleRule.getRuleType());
            targetJob.setComponent(referencedScheduleRule.getComponent());
        } else {
            targetJob.setScheduleRule(null);
            Component referencedComponent = componentRepository.findById(jobDtoSend.getComponentId()).orElseThrow(()
                    -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + jobDtoSend.getComponentId()));
            targetJob.setComponent(referencedComponent);
        }
    }

    private void validateJob(JobDtoSend jobDtoSend) {
        if (jobDtoSend.getScheduleRuleId() != null && jobDtoSend.getType() != null)
            throw new RuntimeException(JOB_TYPE_MUST_BE_NULL_MESSAGE);
        if (jobDtoSend.getScheduleRuleId() != null && jobDtoSend.getComponentId() != null)
            throw new RuntimeException(JOB_COMPONENT_MUST_BE_NULL_MESSAGE);
        if (jobDtoSend.getScheduleRuleId() == null && !jobDtoSend.getType().equalsIgnoreCase(JOB_REPLACE_TYPE)
                && !EnumUtils.isValidEnum(ScheduleRuleTypes.class, jobDtoSend.getType().toUpperCase()))
            throw new RuntimeException(JOB_INVALID_TYPE_FORMAT_MESSAGE);
        if (jobDtoSend.getStatus() != null && !EnumUtils.isValidEnum(JobStatusCategories.class, jobDtoSend.getStatus().toUpperCase()))
            throw new RuntimeException(JOB_INVALID_STATUS_FORMAT_MESSAGE);
        if (jobDtoSend.getWorkedHours() != null)
            for (WorkedHoursDto workedHour : jobDtoSend.getWorkedHours()) {
                if (workedHour.getHoursWorked() < 1) throw new RuntimeException(JOB_INVALID_WORKED_HOURS);
            }
    }

    public enum JobStatusCategories {
        DONE,
        ON_HOLD,
        SUSPENDED_FOR_MISSING_PARTS,
        IN_PROGRESS,
        CANCELLED,
        DELAYED
    }

}

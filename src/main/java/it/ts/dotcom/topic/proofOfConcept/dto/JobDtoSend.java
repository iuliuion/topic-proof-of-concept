package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class JobDtoSend {

    private String name;
    private String type;
    private Long componentId;
    private Long scheduleRuleId;
    private LocalDateTime scheduledTime;
    private List<WorkedHoursDto> workedHours;
    private String status;
    private Boolean crew;
    private Boolean outsourced;
    private Boolean classAttendance;
    private String comments;
    private String attachmentUrl;

}
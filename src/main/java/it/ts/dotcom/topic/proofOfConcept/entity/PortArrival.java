package it.ts.dotcom.topic.proofOfConcept.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_port_arrivals", schema = "poc")
public class PortArrival {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "destination")
    private String destination;
    @Column(name = "arrival_date")
    private LocalDateTime arrivalDate;
    @Column(name = "departure_date")
    private LocalDateTime departureDate;

}

package it.ts.dotcom.topic.proofOfConcept.controller;

import it.ts.dotcom.topic.proofOfConcept.dto.PortArrivalDto;
import it.ts.dotcom.topic.proofOfConcept.dto.PortArrivalDtoSend;
import it.ts.dotcom.topic.proofOfConcept.service.PortArrivalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/portArrival")
public class PortArrivalApi {

    @Autowired
    private PortArrivalService portArrivalService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PortArrivalDto create(@Valid @RequestBody PortArrivalDtoSend portArrivalDtoSend) {
        return portArrivalService.create(portArrivalDtoSend);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<PortArrivalDto> findAll() {
        return portArrivalService.getAll();
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public PortArrivalDto findById(@PathVariable("id") Long id) {
        PortArrivalDto portArrivalDto = portArrivalService.findById(id);
        return portArrivalDto;
    }

    @GetMapping("nextPortArrival")
    @ResponseStatus(HttpStatus.OK)
    public Optional<PortArrivalDto> findNextPortArrival() {
        Optional<PortArrivalDto> portArrivalDto = portArrivalService.findNextPortArrival();
        return portArrivalDto;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long id) {
        portArrivalService.findById(id);
        portArrivalService.deleteById(id);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public PortArrivalDto updateById(@PathVariable("id") Long id, @Valid @RequestBody PortArrivalDtoSend input) {
        return portArrivalService.update(id, input);
    }

    @PostMapping("byExample")
    @ResponseStatus(HttpStatus.OK)
    public List<PortArrivalDto> findAllLikeExample(@Valid @RequestBody PortArrivalDtoSend input) {
        return portArrivalService.getAllLikeExample(input);
    }

}

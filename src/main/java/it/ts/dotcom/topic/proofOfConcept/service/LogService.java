package it.ts.dotcom.topic.proofOfConcept.service;

import com.github.dozermapper.core.Mapper;
import it.ts.dotcom.topic.proofOfConcept.dto.LogDto;
import it.ts.dotcom.topic.proofOfConcept.dto.LogDtoSend;
import it.ts.dotcom.topic.proofOfConcept.entity.Job;
import it.ts.dotcom.topic.proofOfConcept.entity.Log;
import it.ts.dotcom.topic.proofOfConcept.repository.ComponentRepository;
import it.ts.dotcom.topic.proofOfConcept.repository.JobRepository;
import it.ts.dotcom.topic.proofOfConcept.repository.LogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static it.ts.dotcom.topic.proofOfConcept.util.Vars.JOB_NOT_FOUND_MESSAGE;
import static it.ts.dotcom.topic.proofOfConcept.util.Vars.LOG_NOT_FOUND_MESSAGE;

@Service
@Transactional
@Slf4j
public class LogService {

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private ComponentRepository componentRepository;
    @Autowired
    private Mapper beanMapper;

    public List<LogDto> getAll() {
        return logRepository.findAll().stream().map(log -> beanMapper.map(log, LogDto.class)).collect(Collectors.toList());
    }

    public List<LogDto> getAllLikeExample(LogDtoSend logDtoSend) {
        Example<Log> logExample = Example.of(beanMapper.map(logDtoSend, Log.class));
        if (logExample.getProbe().getJob().getId() == null) {
            logExample.getProbe().setJob(null);
        }
        return logRepository.findAll(logExample).stream().map(log
                -> beanMapper.map(log, LogDto.class)).collect(Collectors.toList());
    }

    public LogDto findById(Long id) {
        return beanMapper.map(logRepository.findById(id).orElseThrow(()
                -> new RuntimeException(LOG_NOT_FOUND_MESSAGE + id)), LogDto.class);
    }

    public void deleteById(Long id) {
        logRepository.findById(id).orElseThrow(() -> new RuntimeException(LOG_NOT_FOUND_MESSAGE + id));
        logRepository.deleteById(id);
    }

    public LogDto create(LogDtoSend logDtoSend) {
        Log log = beanMapper.map(logDtoSend, Log.class);
        log.setLogTime(LocalDateTime.now());
        if (logDtoSend.getJobId() != null) {
            Job referencedJob = jobRepository.findById(log.getJob().getId()).orElseThrow(()
                    -> new RuntimeException(JOB_NOT_FOUND_MESSAGE + log.getJob().getId()));
            log.setJob(referencedJob);
        } else log.setJob(null);
        return beanMapper.map(logRepository.save(log), LogDto.class);
    }

    public LogDto update(Long id, LogDtoSend logDtoSend) {
        logRepository.findById(id).orElseThrow(() -> new RuntimeException(LOG_NOT_FOUND_MESSAGE + id));
        Log transitoryLog = new Log();
        transitoryLog.setId(id);
        beanMapper.map(logDtoSend, transitoryLog);
        if (logDtoSend.getJobId() != null) {
            Job referencedJob = jobRepository.findById(transitoryLog.getJob().getId()).orElseThrow(()
                    -> new RuntimeException(JOB_NOT_FOUND_MESSAGE + transitoryLog.getJob().getId()));
            transitoryLog.setJob(referencedJob);
        } else transitoryLog.setJob(null);
        Log log = logRepository.save(transitoryLog);
        LogDto result = beanMapper.map(log, LogDto.class);
        return result;
    }

}

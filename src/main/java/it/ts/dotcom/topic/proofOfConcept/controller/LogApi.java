package it.ts.dotcom.topic.proofOfConcept.controller;

import it.ts.dotcom.topic.proofOfConcept.dto.LogDto;
import it.ts.dotcom.topic.proofOfConcept.dto.LogDtoSend;
import it.ts.dotcom.topic.proofOfConcept.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/log")
public class LogApi {

    @Autowired
    private LogService logService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public LogDto create(@Valid @RequestBody LogDtoSend logDtoSend) {
        return logService.create(logDtoSend);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<LogDto> findAll() {
        return logService.getAll();
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public LogDto findById(@PathVariable("id") Long id) {
        LogDto logDto = logService.findById(id);
        return logDto;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long id) {
        logService.findById(id);
        logService.deleteById(id);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public LogDto updateById(@PathVariable("id") Long id, @Valid @RequestBody LogDtoSend input) {
        return logService.update(id, input);
    }

    @PostMapping("byExample")
    @ResponseStatus(HttpStatus.OK)
    public List<LogDto> findAllLikeExample(@Valid @RequestBody LogDtoSend input) {
        return logService.getAllLikeExample(input);
    }

}

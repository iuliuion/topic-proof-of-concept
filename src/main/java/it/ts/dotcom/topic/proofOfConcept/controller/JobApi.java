package it.ts.dotcom.topic.proofOfConcept.controller;

import it.ts.dotcom.topic.proofOfConcept.dto.JobDto;
import it.ts.dotcom.topic.proofOfConcept.dto.JobDtoSend;
import it.ts.dotcom.topic.proofOfConcept.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/job")
public class JobApi {

    @Autowired
    private JobService jobService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public JobDto create(@Valid @RequestBody JobDtoSend jobDtoSend) {
        return jobService.create(jobDtoSend);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<JobDto> findAll() {
        return jobService.getAll();
    }

    @GetMapping("activeJobs")
    @ResponseStatus(HttpStatus.OK)
    public List<JobDto> findAllActiveJobs() {
        return jobService.getAllActiveJobs();
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public JobDto findById(@PathVariable("id") Long id) {
        JobDto jobDto = jobService.findById(id);
        return jobDto;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long id) {
        jobService.findById(id);
        jobService.deleteById(id);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public JobDto updateById(@PathVariable("id") Long id, @Valid @RequestBody JobDtoSend input) {
        return jobService.update(id, input);
    }

    @PostMapping("byExample")
    @ResponseStatus(HttpStatus.OK)
    public List<JobDto> findAllLikeExample(@Valid @RequestBody JobDtoSend input) {
        return jobService.getAllLikeExample(input);
    }

}

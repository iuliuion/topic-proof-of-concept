package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

@Data
public class ComponentDto {

    private Long id;
    private String name;
    private String description;
    private ComponentDto parentComponent;
    private String area;
    private String referenceId;
    private String status;
    private String comments;
    private String csmItemCode;
    private Boolean logWorkingHours;
    private Long workingHours;

}

package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

@Data
public class ComponentDtoSend {

    private String name;
    private String description;
    private Long parentComponentId;
    private String area;
    private String referenceId;
    private String status;
    private String comments;
    private String csmItemCode;
    private Boolean logWorkingHours;
    private Long workingHours;

}

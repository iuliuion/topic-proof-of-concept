package it.ts.dotcom.topic.proofOfConcept.controller;

import it.ts.dotcom.topic.proofOfConcept.dto.ComponentDto;
import it.ts.dotcom.topic.proofOfConcept.dto.ComponentDtoSend;
import it.ts.dotcom.topic.proofOfConcept.service.ComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/component")
public class ComponentApi {

    @Autowired
    private ComponentService componentService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ComponentDto create(@Valid @RequestBody ComponentDtoSend componentDtoSend) {
        return componentService.create(componentDtoSend);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentDto> findAll() {
        return componentService.getAll();
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public ComponentDto findById(@PathVariable("id") Long id) {
        ComponentDto componentDto = componentService.findById(id);
        return componentDto;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long id) {
        componentService.findById(id);
        componentService.deleteById(id);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public ComponentDto updateById(@PathVariable("id") Long id, @Valid @RequestBody ComponentDtoSend input) {
        return componentService.update(id, input);
    }

    @GetMapping("{id}/potentialChildren")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentDto> findAllPotentialChildrenById(@PathVariable("id") Long id) {
        return componentService.getAllPotentialChildrenById(id);
    }

    @GetMapping("{id}/children")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentDto> findAllChildrenById(@PathVariable("id") Long id) {
        return componentService.getAllChildrenById(id);
    }

    @PostMapping("byExample")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentDto> findAllLikeExample(@Valid @RequestBody ComponentDtoSend input) {
        return componentService.getAllLikeExample(input);
    }

}

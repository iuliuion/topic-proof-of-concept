package it.ts.dotcom.topic.proofOfConcept.service;

import com.github.dozermapper.core.Mapper;
import it.ts.dotcom.topic.proofOfConcept.dto.ComponentDto;
import it.ts.dotcom.topic.proofOfConcept.dto.ComponentDtoSend;
import it.ts.dotcom.topic.proofOfConcept.entity.Component;
import it.ts.dotcom.topic.proofOfConcept.entity.ComponentWorkingHours;
import it.ts.dotcom.topic.proofOfConcept.repository.ComponentRepository;
import it.ts.dotcom.topic.proofOfConcept.repository.ComponentWorkingHoursRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static it.ts.dotcom.topic.proofOfConcept.util.Vars.*;

@Service
@Transactional
@Slf4j
public class ComponentService {

    @Autowired
    private ComponentRepository componentRepository;
    @Autowired
    private ComponentWorkingHoursRepository componentWorkingHoursRepository;
    @Autowired
    private Mapper beanMapper;

    public List<ComponentDto> getAll() {
        return componentRepository.findAll().stream().map(component
                -> beanMapper.map(component, ComponentDto.class)).collect(Collectors.toList());
    }

    public List<ComponentDto> getAllLikeExample(ComponentDtoSend componentDtoSend) {
        Example<Component> componentExample = Example.of(beanMapper.map(componentDtoSend, Component.class));
        if (componentExample.getProbe().getParentComponent().getId() == null) {
            componentExample.getProbe().setParentComponent(null);
        }
        return componentRepository.findAll(componentExample).stream().map(component
                -> beanMapper.map(component, ComponentDto.class)).collect(Collectors.toList());
    }

    public ComponentDto findById(Long id) {
        return beanMapper.map(componentRepository.findById(id).orElseThrow(()
                -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + id)), ComponentDto.class);
    }

    public void deleteById(Long id) {
        componentRepository.findById(id).orElseThrow(() -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + id));
        componentRepository.deleteById(id);
    }

    public ComponentDto create(ComponentDtoSend componentDtoSend) {
        validateComponent(componentDtoSend);
        Component component = beanMapper.map(componentDtoSend, Component.class);
        if (componentDtoSend.getParentComponentId() != null) {
            Component parentComponent = componentRepository.findById(componentDtoSend.getParentComponentId()).orElseThrow(()
                    -> new RuntimeException(PARENT_COMPONENT_NOT_FOUND_MESSAGE + componentDtoSend.getParentComponentId()));
            component.setParentComponent(parentComponent);
        } else component.setParentComponent(null);
        Component savedComponent = componentRepository.save(component);
        if (componentDtoSend.getLogWorkingHours()) {
            ComponentWorkingHours componentWorkingHours = new ComponentWorkingHours();
            componentWorkingHours.setCreationTime(LocalDateTime.now());
            componentWorkingHours.setComponent(savedComponent);
            componentWorkingHours.setWorkingHours(savedComponent.getWorkingHours());
            componentWorkingHoursRepository.save(componentWorkingHours);
        }
        return beanMapper.map(savedComponent, ComponentDto.class);
    }

    public ComponentDto update(Long id, ComponentDtoSend componentDtoSend) {
        validateComponent(componentDtoSend);
        componentRepository.findById(id).orElseThrow(() -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + id));
        Component transitoryComponent = new Component();
        transitoryComponent.setId(id);
        beanMapper.map(componentDtoSend, transitoryComponent);
        if (componentDtoSend.getParentComponentId() != null) {
            Component parentComponent = componentRepository.findById(componentDtoSend.getParentComponentId()).orElseThrow(()
                    -> new RuntimeException(PARENT_COMPONENT_NOT_FOUND_MESSAGE + componentDtoSend.getParentComponentId()));
            transitoryComponent.setParentComponent(parentComponent);
        } else transitoryComponent.setParentComponent(null);
        Component component = componentRepository.save(transitoryComponent);
        ComponentDto result = beanMapper.map(component, ComponentDto.class);
        if (componentDtoSend.getLogWorkingHours()) {
            componentRepository.updateComponentAndChildrenWorkingHoursById(result.getId(), componentDtoSend.getWorkingHours());
        }
        return result;
    }

    public List<ComponentDto> getAllPotentialChildrenById(Long id) {
        componentRepository.findById(id).orElseThrow(() -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + id));
        return componentRepository.findAllPotentialChildren(id).stream().map(component
                -> beanMapper.map(component, ComponentDto.class)).collect(Collectors.toList());
    }

    public List<ComponentDto> getAllChildrenById(Long id) {
        componentRepository.findById(id).orElseThrow(() -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + id));
        return componentRepository.findAllByParentComponentId(id).stream().map(component
                -> beanMapper.map(component, ComponentDto.class)).collect(Collectors.toList());
    }

    private void validateComponent(ComponentDtoSend componentDtoSend) {
        if ((componentDtoSend.getLogWorkingHours() && componentDtoSend.getWorkingHours() == null)
                || (!componentDtoSend.getLogWorkingHours() && componentDtoSend.getWorkingHours() != null))
            throw new RuntimeException(COMPONENT_WORKING_HOURS_INVALID_MESSAGE);
    }

}

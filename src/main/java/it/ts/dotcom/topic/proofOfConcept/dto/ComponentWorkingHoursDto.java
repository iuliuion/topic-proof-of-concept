package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ComponentWorkingHoursDto {

    private Long id;
    private ComponentDto componentDto;
    private LocalDateTime creationTime;
    private Long workingHours;
    private String comments;

}

package it.ts.dotcom.topic.proofOfConcept.service;

public enum ScheduleRuleFrequencyTypes {

    EACH_DAYS_INTERVAL,
    EACH_WORKING_HOURS_INTERVAL,
    EACH_DAYS_OR_WORKING_HOURS_INTERVAL,
    EACH_PORT_DOCK_STOP

}

package it.ts.dotcom.topic.proofOfConcept.controller;

import it.ts.dotcom.topic.proofOfConcept.dto.ScheduleRuleDto;
import it.ts.dotcom.topic.proofOfConcept.dto.ScheduleRuleDtoSend;
import it.ts.dotcom.topic.proofOfConcept.service.ScheduleRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/scheduleRule")
public class ScheduleRuleApi {

    @Autowired
    private ScheduleRuleService scheduleRuleService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ScheduleRuleDto create(@Valid @RequestBody ScheduleRuleDtoSend scheduleRuleDtoSend) {
        return scheduleRuleService.create(scheduleRuleDtoSend);
    }

    @GetMapping("{id}/duplicateFor{componentId}")
    @ResponseStatus(HttpStatus.OK)
    public ScheduleRuleDto duplicateScheduleRuleForComponent(@PathVariable("id") Long id, @PathVariable("componentId") Long componentId) {
        return scheduleRuleService.duplicateScheduleRuleForComponent(id, componentId);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ScheduleRuleDto> findAll() {
        return scheduleRuleService.getAll();
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public ScheduleRuleDto findById(@PathVariable("id") Long id) {
        ScheduleRuleDto scheduleRuleDto = scheduleRuleService.findById(id);
        return scheduleRuleDto;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long id) {
        scheduleRuleService.findById(id);
        scheduleRuleService.deleteById(id);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public ScheduleRuleDto updateById(@PathVariable("id") Long id, @Valid @RequestBody ScheduleRuleDtoSend input) {
        return scheduleRuleService.update(id, input);
    }

    @PutMapping("{id}/disable")
    @ResponseStatus(HttpStatus.OK)
    public ScheduleRuleDto disableById(@PathVariable("id") Long id) {
        return scheduleRuleService.disable(id);
    }

    @PostMapping("byExample")
    @ResponseStatus(HttpStatus.OK)
    public List<ScheduleRuleDto> findAllLikeExample(@Valid @RequestBody ScheduleRuleDtoSend input) {
        return scheduleRuleService.getAllLikeExample(input);
    }

}

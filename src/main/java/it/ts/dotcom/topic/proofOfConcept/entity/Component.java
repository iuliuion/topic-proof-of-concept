package it.ts.dotcom.topic.proofOfConcept.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_components", schema = "poc")
@SqlResultSetMapping(
        name = "ComponentFindAllPotentialChildrenResult",
        entities = {
                @EntityResult(
                        entityClass = Component.class,
                        fields = {
                                @FieldResult(name = "id", column = "id"),
                                @FieldResult(name = "name", column = "name"),
                                @FieldResult(name = "description", column = "description"),
                                @FieldResult(name = "area", column = "area"),
                                @FieldResult(name = "referenceId", column = "reference_id"),
                                @FieldResult(name = "parentComponent", column = "parent_component_id"),
                                @FieldResult(name = "status", column = "status"),
                                @FieldResult(name = "comments", column = "comments"),
                                @FieldResult(name = "csmItemCode", column = "csm_item_code"),
                                @FieldResult(name = "logWorkingHours", column = "log_working_hours"),
                                @FieldResult(name = "workingHours", column = "working_hours")
                        })})
@NamedNativeQuery(
        name = "Component.findAllPotentialChildren",
        query = "WITH root_element AS " +
                "(" +
                "   WITH RECURSIVE parents(id, parent_component_id) AS " +
                "   (" +
                "      SELECT" +
                "         id," +
                "         parent_component_id " +
                "      FROM" +
                "         poc.t_components " +
                "      WHERE" +
                "         poc.t_components.id = :id " +
                "      UNION ALL" +
                "      SELECT" +
                "         tc.id," +
                "         tc.parent_component_id " +
                "      FROM" +
                "         parents p," +
                "         poc.t_components tc " +
                "      WHERE" +
                "         p.parent_component_id = tc.id " +
                "   )" +
                "   SELECT" +
                "      * " +
                "   FROM" +
                "      parents " +
                "   WHERE" +
                "      parent_component_id IS NULL " +
                ")" +
                "SELECT" +
                "   tc2.*," +
                "   (" +
                "      SELECT" +
                "         tcwh.working_hours " +
                "      FROM" +
                "         poc.t_components_working_hours tcwh " +
                "      WHERE" +
                "         tcwh.component_id = tc2.id " +
                "      ORDER BY" +
                "         tcwh.creation_time DESC LIMIT 1 " +
                "   )" +
                "FROM" +
                "   poc.t_components tc2," +
                "   root_element " +
                "WHERE" +
                "   tc2.id != root_element.id " +
                "   AND tc2.parent_component_id IS NULL " +
                "   AND tc2.id != :id",
        resultSetMapping = "ComponentFindAllPotentialChildrenResult")
public class Component {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(name = "parent_component_id")
    private Component parentComponent;
    @Column(name = "area")
    private String area;
    @Column(name = "reference_id")
    private String referenceId;
    @Column(name = "status")
    private String status;
    @Column(name = "comments")
    private String comments;
    @Column(name = "csm_item_code")
    private String csmItemCode;
    @Column(name = "log_working_hours")
    private Boolean logWorkingHours;
    @Formula("(select tcwh.working_hours from poc.t_components_working_hours tcwh " +
            "where tcwh.component_id = id order by tcwh.creation_time desc limit 1 )")
    private Long workingHours;

}

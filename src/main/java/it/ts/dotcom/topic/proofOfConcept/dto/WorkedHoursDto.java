package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class WorkedHoursDto {

    LocalDateTime date;
    Long hoursWorked;
    String workerName;
    String notes;

}

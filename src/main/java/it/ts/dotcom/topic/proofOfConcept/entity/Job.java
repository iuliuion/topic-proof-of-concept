package it.ts.dotcom.topic.proofOfConcept.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_jobs", schema = "poc")
@TypeDef(
        name = "jsonb",
        typeClass = JsonBinaryType.class)
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "type")
    private String type;
    @ManyToOne
    @JoinColumn(name = "component_id")
    private Component component;
    @ManyToOne
    @JoinColumn(name = "schedule_rule_id")
    private ScheduleRule scheduleRule;
    @Column(name = "scheduled_time")
    private LocalDateTime scheduledTime;
    @Column(name = "completion_time")
    private LocalDateTime completionTime;
    @Type(type = "jsonb")
    @Column(name = "worked_hours", columnDefinition = "jsonb")
    private String workedHours;
    @Column(name = "status")
    private String status;
    @Column(name = "crew")
    private Boolean crew;
    @Column(name = "outsourced")
    private Boolean outsourced;
    @Column(name = "class_attendance")
    private Boolean classAttendance;
    @Column(name = "comments")
    private String comments;
    @Column(name = "attachment_url")
    private String attachmentUrl;

}

package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LogDto {

    private Long id;
    private String crewMember;
    private JobDto job;
    private LocalDateTime logTime;
    private String eventLogged;
    private String attachmentUrl;

}

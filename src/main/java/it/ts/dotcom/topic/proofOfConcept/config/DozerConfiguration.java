package it.ts.dotcom.topic.proofOfConcept.config;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.github.dozermapper.core.loader.api.BeanMappingBuilder;
import it.ts.dotcom.topic.proofOfConcept.dto.*;
import it.ts.dotcom.topic.proofOfConcept.entity.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.github.dozermapper.core.loader.api.FieldsMappingOptions.customConverter;

@Configuration
public class DozerConfiguration {
    @Bean
    public Mapper beanMapper() {
        return DozerBeanMapperBuilder.create()
                .withMappingBuilders(new BeanMappingBuilder() {
                    @Override
                    protected void configure() {

                        mapping(ComponentDtoSend.class, Component.class)
                                .fields("parentComponentId", "parentComponent.id");
                        mapping(ComponentWorkingHoursDtoSend.class, ComponentWorkingHours.class)
                                .fields("componentId", "component.id");
                        mapping(ScheduleRuleDtoSend.class, ScheduleRule.class)
                                .fields("componentId", "component.id");
                        mapping(LogDtoSend.class, Log.class)
                                .fields("jobId", "job.id");
                        mapping(JobDtoSend.class, Job.class)
                                .fields("scheduleRuleId", "scheduleRule.id")
                                .fields("componentId", "component.id")
                                .fields("workedHours", "workedHours",
                                        customConverter("it.ts.dotcom.topic.proofOfConcept.util.MyJsonConverter"));
                        mapping(JobDto.class, Job.class)
                                .fields("workedHours", "workedHours",
                                        customConverter("it.ts.dotcom.topic.proofOfConcept.util.MyJsonConverter"));

                    }
                })
                .build();
    }
}

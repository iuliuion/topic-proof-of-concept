package it.ts.dotcom.topic.proofOfConcept.repository;

import it.ts.dotcom.topic.proofOfConcept.entity.PortArrival;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PortArrivalRepository extends JpaRepository<PortArrival, Long> {

    @Query(
            value = "select" +
                    "   * " +
                    "from" +
                    "   poc.t_port_arrivals tpa " +
                    "where" +
                    "   tpa.arrival_date > current_timestamp " +
                    "order by" +
                    "   tpa.arrival_date asc limit 1",
            nativeQuery = true)
    Optional<PortArrival> getNextPortArrival();

}

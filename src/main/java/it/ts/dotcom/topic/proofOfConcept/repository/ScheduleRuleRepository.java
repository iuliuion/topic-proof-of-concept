package it.ts.dotcom.topic.proofOfConcept.repository;

import it.ts.dotcom.topic.proofOfConcept.entity.ScheduleRule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ScheduleRuleRepository extends JpaRepository<ScheduleRule, Long> {

    @Query(
            value = "select" +
                    "   * " +
                    "from" +
                    "   poc.t_schedule_rules tsr " +
                    "where" +
                    "   tsr.start_date <= current_timestamp " +
                    "   and tsr.end_date is null",
            nativeQuery = true)
    List<ScheduleRule> findAllActiveRules();

    @Modifying
    @Query(
            value = "WITH cancelled_rule_jobs AS " +
                    "(" +
                    "   UPDATE" +
                    "      poc.t_jobs tj " +
                    "   SET" +
                    "      status = 'CANCELLED', completion_time = CURRENT_TIMESTAMP " +
                    "   WHERE" +
                    "      tj.schedule_rule_id = :id " +
                    "      AND completion_time IS NULL " +
                    "      AND tj.status IS NULL returning tj.id, tj.completion_time, tj.status " +
                    ")" +
                    "INSERT INTO" +
                    "   poc.t_logs(crew_member, log_time , job_id , event_logged) " +
                    "   SELECT DISTINCT" +
                    "      'SYSTEM'," +
                    "      cancelled_rule_jobs.completion_time," +
                    "      cancelled_rule_jobs.id," +
                    "      cancelled_rule_jobs.status " +
                    "   FROM" +
                    "      cancelled_rule_jobs",
            nativeQuery = true)
    void cancelUnfinishedRuleJobsByRuleId(Long id);

}

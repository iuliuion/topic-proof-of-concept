package it.ts.dotcom.topic.proofOfConcept.util;

public class Vars {

    public static final String COMPONENT_NOT_FOUND_MESSAGE = "no Component object found with given Id: ";
    public static final String PARENT_COMPONENT_NOT_FOUND_MESSAGE = "no Component object found with Id equivalent to the given parentComponentId: ";
    public static final String COMPONENT_WORKING_HOURS_INVALID_MESSAGE = "Component.workingHours field must be completed when Component.logWorkingHours is set as true, and not completed otherwise";

    public static final String SCHEDULE_RULE_NOT_FOUND_MESSAGE = "no ScheduleRule object found with given Id: ";
    public static final String SCHEDULE_RULE_INVALID_RULE_TYPE_FORMAT_MESSAGE = "the value of the ScheduleRule.ruleType field wasn't found in the recognized schedule rule categories";
    public static final String SCHEDULE_RULE_INVALID_FREQUENCY_TYPE_FORMAT_MESSAGE = "the value of the ScheduleRule.frequencyType field wasn't found among the recognized rule types";
    public static final String SCHEDULE_RULE_DAYS_FREQUENCY_INVALID_MESSAGE = "ScheduleRule.frequency field is indicating a days based interval but ScheduleRule.daysFrequency field isn't completed with a valid value";
    public static final String SCHEDULE_RULE_DAYS_FREQUENCY_MUST_BE_NULL_MESSAGE = "ScheduleRule.daysFrequency field must not be completed if ScheduleRule.frequency isn't indicating a days based interval";
    public static final String SCHEDULE_RULE_HOURS_FREQUENCY_INVALID_MESSAGE = "ScheduleRule.frequency field is indicating a working hours based interval but ScheduleRule.workingHoursFrequency field isn't completed with a valid value";
    public static final String SCHEDULE_RULE_HOURS_FREQUENCY_MUST_BE_NULL_MESSAGE = "ScheduleRule.workingHoursFrequency field must not be completed if ScheduleRule.frequency isn't indicating a working hours based interval";

    public static final String JOB_NOT_FOUND_MESSAGE = "no Job object found with given Id: ";
    public static final String JOB_TYPE_MUST_BE_NULL_MESSAGE = "if Job.scheduleRuleId is completed the Job.type must not be completed since it will be acquired from the referenced ScheduleRule";
    public static final String JOB_COMPONENT_MUST_BE_NULL_MESSAGE = "if Job.scheduleRuleId is completed the Job.componentId must not be completed since it will be acquired from the referenced ScheduleRule";
    public static final String JOB_INVALID_TYPE_FORMAT_MESSAGE = "the value of the Job.type field wasn't found among the recognized types";
    public static final String JOB_INVALID_STATUS_FORMAT_MESSAGE = "the value of the Job.status field must be part of the recognized possible status values or must not be completed";
    public static final String JOB_INVALID_WORKED_HOURS = "a Job.hoursWorked field isn't completed with a valid value";
    public static final String JOB_REPLACE_TYPE = "REPLACE";

    public static final String LOG_NOT_FOUND_MESSAGE = "no Log object found with given Id: ";

    public static final String PORT_ARRIVAL_NOT_FOUND_MESSAGE = "no PortArrival found with given Id: ";
    public static final String PORT_ARRIVAL_LAST_DEPARTURE_DATE_INVALID_MESSAGE = "to insert a new PortArrival the last PortArrival must have the PortArrival.departureDate field completed";
    public static final String PORT_ARRIVAL_DEPARTURE_DATE_MUST_BE_NULL_MESSAGE = "PortArrival.departureDate field must not be completed if PortArrival.arrivalDate isn't completed";
    public static final String PORT_ARRIVAL_DEPARTURE_DATE_INVALID_MESSAGE = "PortArrival.departureDate field must indicate a date previous to PortArrival.arrivalDate";

}

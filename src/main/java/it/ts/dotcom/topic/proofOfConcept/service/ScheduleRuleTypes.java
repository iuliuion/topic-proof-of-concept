package it.ts.dotcom.topic.proofOfConcept.service;

public enum ScheduleRuleTypes {

    INSPECT,
    MEASURE,
    CLEAN,
    REPAIR

}

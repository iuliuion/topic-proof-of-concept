package it.ts.dotcom.topic.proofOfConcept.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.dozermapper.core.DozerConverter;
import it.ts.dotcom.topic.proofOfConcept.dto.WorkedHoursDto;
import lombok.SneakyThrows;

import java.util.List;

public class MyJsonConverter extends DozerConverter<List, String> {

    ObjectMapper jsonMapper = new ObjectMapper();

    public MyJsonConverter() {
        super(List.class, String.class);
        jsonMapper.registerModule(new JavaTimeModule());
    }

    @SneakyThrows
    @Override
    public String convertTo(List list, String jsonString) {
        if (list == null) return null;
        return jsonMapper.writeValueAsString(list);
    }

    @Override
    public List convertFrom(String jsonString, List list) {
        if (jsonString == null) return null;
        try {
            return jsonMapper.readValue(jsonString, new TypeReference<List<WorkedHoursDto>>() {
            });
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}


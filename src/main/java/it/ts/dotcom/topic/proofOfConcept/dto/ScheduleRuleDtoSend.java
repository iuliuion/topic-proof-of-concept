package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ScheduleRuleDtoSend {

    private String name;
    private Long componentId;
    private String ruleType;
    private LocalDateTime startDate;
    private String frequencyType;
    private Long daysFrequency;
    private Long workingHoursFrequency;
    private String description;

}
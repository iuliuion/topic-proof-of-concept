package it.ts.dotcom.topic.proofOfConcept.repository;

import it.ts.dotcom.topic.proofOfConcept.entity.Component;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ComponentRepository extends JpaRepository<Component, Long> {

    List<Component> findAllPotentialChildren(Long id);

    List<Component> findAllByParentComponentId(Long id);

    @Query(
            value = "WITH relevant_components AS " +
                    "(" +
                    "   SELECT" +
                    "      tc.id " +
                    "   FROM" +
                    "      poc.t_components tc " +
                    "   WHERE" +
                    "      tc.parent_component_id = :id " +
                    "      OR tc.id = :id " +
                    ")" +
                    "INSERT INTO" +
                    "   poc.t_components_working_hours (component_id, creation_time, working_hours) " +
                    "   SELECT DISTINCT" +
                    "      relevant_components.id," +
                    "      CURRENT_TIMESTAMP," +
                    "      :workingHours " +
                    "   FROM" +
                    "      relevant_components",
            nativeQuery = true)
    @Modifying
    void updateComponentAndChildrenWorkingHoursById(Long id, Long workingHours);

}

package it.ts.dotcom.topic.proofOfConcept.service;

import com.github.dozermapper.core.Mapper;
import it.ts.dotcom.topic.proofOfConcept.dto.ScheduleRuleDto;
import it.ts.dotcom.topic.proofOfConcept.dto.ScheduleRuleDtoSend;
import it.ts.dotcom.topic.proofOfConcept.entity.Component;
import it.ts.dotcom.topic.proofOfConcept.entity.ScheduleRule;
import it.ts.dotcom.topic.proofOfConcept.repository.ComponentRepository;
import it.ts.dotcom.topic.proofOfConcept.repository.ScheduleRuleRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static it.ts.dotcom.topic.proofOfConcept.util.Vars.*;

@Service
@Transactional
@Slf4j
public class ScheduleRuleService {

    @Autowired
    private ScheduleRuleRepository scheduleRuleRepository;
    @Autowired
    private ComponentRepository componentRepository;
    @Autowired
    private Mapper beanMapper;

    public List<ScheduleRuleDto> getAll() {
        return scheduleRuleRepository.findAll().stream().map(scheduleRule
                -> beanMapper.map(scheduleRule, ScheduleRuleDto.class)).collect(Collectors.toList());
    }

    public List<ScheduleRuleDto> getAllLikeExample(ScheduleRuleDtoSend scheduleRuleDtoSend) {
        Example<ScheduleRule> scheduleRuleExample = Example.of(beanMapper.map(scheduleRuleDtoSend, ScheduleRule.class));
        if (scheduleRuleExample.getProbe().getComponent().getId() == null) {
            scheduleRuleExample.getProbe().setComponent(null);
        }
        return scheduleRuleRepository.findAll(scheduleRuleExample).stream().map(scheduleRule
                -> beanMapper.map(scheduleRule, ScheduleRuleDto.class)).collect(Collectors.toList());
    }

    public ScheduleRuleDto findById(Long id) {
        return beanMapper.map(scheduleRuleRepository.findById(id).orElseThrow(()
                -> new RuntimeException(SCHEDULE_RULE_NOT_FOUND_MESSAGE + id)), ScheduleRuleDto.class);
    }

    public void deleteById(Long id) {
        scheduleRuleRepository.findById(id).orElseThrow(() -> new RuntimeException(SCHEDULE_RULE_NOT_FOUND_MESSAGE + id));
        scheduleRuleRepository.deleteById(id);
    }

    public ScheduleRuleDto create(ScheduleRuleDtoSend scheduleRuleDtoSend) {
        validateScheduleRule(scheduleRuleDtoSend);
        ScheduleRule scheduleRule = beanMapper.map(scheduleRuleDtoSend, ScheduleRule.class);
        Component referencedComponent = componentRepository.findById(scheduleRule.getComponent().getId()).orElseThrow(()
                -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + scheduleRule.getComponent().getId()));
        scheduleRule.setComponent(referencedComponent);
        return beanMapper.map(scheduleRuleRepository.save(scheduleRule), ScheduleRuleDto.class);
    }

    public ScheduleRuleDto duplicateScheduleRuleForComponent(Long oldScheduleRuleId, Long componentId) {
        Component component = componentRepository.findById(componentId).orElseThrow(()
                -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + componentId));
        ScheduleRule scheduleRule = scheduleRuleRepository.findById(oldScheduleRuleId).orElseThrow(()
                -> new RuntimeException(SCHEDULE_RULE_NOT_FOUND_MESSAGE + oldScheduleRuleId));
        ScheduleRule newScheduleRule = beanMapper.map(scheduleRule, ScheduleRule.class);
        newScheduleRule.setId(null);
        newScheduleRule.setComponent(component);
        newScheduleRule.setStartDate(LocalDateTime.now());
        newScheduleRule.setEndDate(null);
        newScheduleRule.setDescription(null);
        return beanMapper.map(scheduleRuleRepository.save(newScheduleRule), ScheduleRuleDto.class);
    }


    public ScheduleRuleDto update(Long id, ScheduleRuleDtoSend scheduleRuleDtoSend) {
        validateScheduleRule(scheduleRuleDtoSend);
        ScheduleRule scheduleRule = scheduleRuleRepository.findById(id).orElseThrow(()
                -> new RuntimeException(SCHEDULE_RULE_NOT_FOUND_MESSAGE + id));
        ScheduleRule transitoryScheduleRule = beanMapper.map(scheduleRule, ScheduleRule.class);
        beanMapper.map(scheduleRuleDtoSend, transitoryScheduleRule);
        componentRepository.findById(transitoryScheduleRule.getComponent().getId()).orElseThrow(()
                -> new RuntimeException(COMPONENT_NOT_FOUND_MESSAGE + transitoryScheduleRule.getComponent().getId()));
        ScheduleRule updatedSchedule = scheduleRuleRepository.save(transitoryScheduleRule);
        ScheduleRuleDto result = beanMapper.map(updatedSchedule, ScheduleRuleDto.class);
        return result;
    }

    public ScheduleRuleDto disable(Long id) {
        ScheduleRule scheduleRule = scheduleRuleRepository.findById(id).orElseThrow(()
                -> new RuntimeException(SCHEDULE_RULE_NOT_FOUND_MESSAGE + id));
        scheduleRule.setEndDate(LocalDateTime.now());
        ScheduleRule disabledScheduleRule = scheduleRuleRepository.save(scheduleRule);
        scheduleRuleRepository.cancelUnfinishedRuleJobsByRuleId(id);
        return beanMapper.map(disabledScheduleRule, ScheduleRuleDto.class);
    }

    private void validateScheduleRule(ScheduleRuleDtoSend scheduleRuleDtoSend) {
        if (!EnumUtils.isValidEnum(ScheduleRuleTypes.class, scheduleRuleDtoSend.getRuleType().toUpperCase()))
            throw new RuntimeException(SCHEDULE_RULE_INVALID_RULE_TYPE_FORMAT_MESSAGE);
        if (!EnumUtils.isValidEnum(ScheduleRuleFrequencyTypes.class, scheduleRuleDtoSend.getFrequencyType().toUpperCase()))
            throw new RuntimeException(SCHEDULE_RULE_INVALID_FREQUENCY_TYPE_FORMAT_MESSAGE);

        ScheduleRuleFrequencyTypes scheduleRuleFrequencyType = ScheduleRuleFrequencyTypes
                .valueOf(scheduleRuleDtoSend.getFrequencyType());
        switch (scheduleRuleFrequencyType) {
            case EACH_DAYS_OR_WORKING_HOURS_INTERVAL: {
                if (scheduleRuleDtoSend.getDaysFrequency() == null || scheduleRuleDtoSend.getDaysFrequency() < 1)
                    throw new RuntimeException(SCHEDULE_RULE_DAYS_FREQUENCY_INVALID_MESSAGE);
                if (scheduleRuleDtoSend.getWorkingHoursFrequency() == null || scheduleRuleDtoSend.getWorkingHoursFrequency() < 1)
                    throw new RuntimeException(SCHEDULE_RULE_HOURS_FREQUENCY_INVALID_MESSAGE);
                break;
            }
            case EACH_DAYS_INTERVAL: {
                if (scheduleRuleDtoSend.getDaysFrequency() == null || scheduleRuleDtoSend.getDaysFrequency() < 1)
                    throw new RuntimeException(SCHEDULE_RULE_DAYS_FREQUENCY_INVALID_MESSAGE);
                if (scheduleRuleDtoSend.getWorkingHoursFrequency() != null)
                    throw new RuntimeException(SCHEDULE_RULE_HOURS_FREQUENCY_MUST_BE_NULL_MESSAGE);
                break;
            }
            case EACH_WORKING_HOURS_INTERVAL: {
                if (scheduleRuleDtoSend.getWorkingHoursFrequency() == null || scheduleRuleDtoSend.getWorkingHoursFrequency() < 1)
                    throw new RuntimeException(SCHEDULE_RULE_HOURS_FREQUENCY_INVALID_MESSAGE);
                if (scheduleRuleDtoSend.getDaysFrequency() != null)
                    throw new RuntimeException(SCHEDULE_RULE_DAYS_FREQUENCY_MUST_BE_NULL_MESSAGE);
                break;
            }
            case EACH_PORT_DOCK_STOP: {
                if (scheduleRuleDtoSend.getDaysFrequency() != null)
                    throw new RuntimeException(SCHEDULE_RULE_DAYS_FREQUENCY_MUST_BE_NULL_MESSAGE);
                if (scheduleRuleDtoSend.getWorkingHoursFrequency() != null)
                    throw new RuntimeException(SCHEDULE_RULE_HOURS_FREQUENCY_MUST_BE_NULL_MESSAGE);
                break;
            }
        }
    }

}

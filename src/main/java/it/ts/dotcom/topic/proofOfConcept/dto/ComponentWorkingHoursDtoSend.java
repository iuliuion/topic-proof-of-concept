package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

@Data
public class ComponentWorkingHoursDtoSend {

    private Long componentId;
    private Long workingHours;
    private String comments;

}

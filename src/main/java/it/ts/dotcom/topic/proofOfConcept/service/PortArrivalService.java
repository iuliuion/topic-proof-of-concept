package it.ts.dotcom.topic.proofOfConcept.service;

import com.github.dozermapper.core.Mapper;
import it.ts.dotcom.topic.proofOfConcept.dto.PortArrivalDto;
import it.ts.dotcom.topic.proofOfConcept.dto.PortArrivalDtoSend;
import it.ts.dotcom.topic.proofOfConcept.entity.PortArrival;
import it.ts.dotcom.topic.proofOfConcept.repository.PortArrivalRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static it.ts.dotcom.topic.proofOfConcept.util.Vars.*;

@Service
@Transactional
@Slf4j
public class PortArrivalService {

    @Autowired
    private PortArrivalRepository portArrivalRepository;
    @Autowired
    private Mapper beanMapper;

    public List<PortArrivalDto> getAll() {
        return portArrivalRepository.findAll().stream().map(portArrival
                -> beanMapper.map(portArrival, PortArrivalDto.class)).collect(Collectors.toList());
    }

    public List<PortArrivalDto> getAllLikeExample(PortArrivalDtoSend portArrivalDtoSend) {
        Example<PortArrival> portArrivalExample = Example.of(beanMapper.map(portArrivalDtoSend, PortArrival.class));
        return portArrivalRepository.findAll(portArrivalExample).stream().map(portArrival
                -> beanMapper.map(portArrival, PortArrivalDto.class)).collect(Collectors.toList());
    }

    public PortArrivalDto findById(Long id) {
        return beanMapper.map(portArrivalRepository.findById(id).orElseThrow(()
                -> new RuntimeException(PORT_ARRIVAL_NOT_FOUND_MESSAGE + id)), PortArrivalDto.class);
    }

    public Optional<PortArrivalDto> findNextPortArrival() {
        return portArrivalRepository.getNextPortArrival().map(portArrival -> beanMapper.map(portArrival, PortArrivalDto.class));
    }

    public void deleteById(Long id) {
        portArrivalRepository.findById(id).orElseThrow(() -> new RuntimeException(PORT_ARRIVAL_NOT_FOUND_MESSAGE + id));
        portArrivalRepository.deleteById(id);
    }

    public PortArrivalDto create(PortArrivalDtoSend portArrivalDtoSend) {
        validatePortArrival(portArrivalDtoSend);
        PortArrival portArrival = beanMapper.map(portArrivalDtoSend, PortArrival.class);
        Optional<PortArrival> latestPortArrival = portArrivalRepository.getNextPortArrival();
        if (latestPortArrival.isPresent()) {
            if (latestPortArrival.get().getDepartureDate() == null)
                throw new RuntimeException(PORT_ARRIVAL_LAST_DEPARTURE_DATE_INVALID_MESSAGE);
        }
        return beanMapper.map(portArrivalRepository.save(portArrival), PortArrivalDto.class);
    }

    public PortArrivalDto update(Long id, PortArrivalDtoSend portArrivalDtoSend) {
        validatePortArrival(portArrivalDtoSend);
        PortArrival portArrival = portArrivalRepository.findById(id).orElseThrow(()
                -> new RuntimeException(PORT_ARRIVAL_NOT_FOUND_MESSAGE + id));
        beanMapper.map(portArrivalDtoSend, portArrival);
        PortArrival updatedPortArrival = portArrivalRepository.save(portArrival);
        PortArrivalDto result = beanMapper.map(updatedPortArrival, PortArrivalDto.class);
        return result;
    }

    private void validatePortArrival(PortArrivalDtoSend portArrivalDtoSend) {
        if (portArrivalDtoSend.getArrivalDate() == null) {
            if (portArrivalDtoSend.getDepartureDate() != null)
                throw new RuntimeException(PORT_ARRIVAL_DEPARTURE_DATE_MUST_BE_NULL_MESSAGE);
        } else if ((portArrivalDtoSend.getDepartureDate() != null)
                && (portArrivalDtoSend.getDepartureDate().isBefore(portArrivalDtoSend.getArrivalDate())))
            throw new RuntimeException(PORT_ARRIVAL_DEPARTURE_DATE_INVALID_MESSAGE);
    }

}

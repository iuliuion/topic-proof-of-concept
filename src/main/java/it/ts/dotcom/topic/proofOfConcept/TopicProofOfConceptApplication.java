package it.ts.dotcom.topic.proofOfConcept;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicProofOfConceptApplication {

    public static void main(String[] args) {
        SpringApplication.run(TopicProofOfConceptApplication.class, args);
    }

}

package it.ts.dotcom.topic.proofOfConcept.dto;

import lombok.Data;

@Data
public class LogDtoSend {

    private String crewMember;
    private Long jobId;
    private String eventLogged;
    private String attachmentUrl;

}

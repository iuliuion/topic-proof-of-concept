
CREATE TABLE t_components (
id serial PRIMARY KEY,
name VARCHAR NOT NULL,
description varchar(1024),
parent_component_id integer REFERENCES t_components(id),
area VARCHAR,
reference_id varchar NOT NULL UNIQUE,
status varchar NOT NULL,
comments varchar(2048),
csm_item_code varchar UNIQUE,
log_working_hours boolean NOT NULL
);

INSERT INTO t_components (name, parent_component_id, reference_id, status, csm_item_code, log_working_hours) VALUES
    ('engine1', null, '1', 'ACTIVE', 'AAA', true),
    ('engine2', null, '2', 'ACTIVE', 'BAA', true),
    ('fan1', null, '3', 'ACTIVE', 'CAA', true),
    ('screw1', 1, '4', 'ACTIVE', null, true),
    ('screw2', 1, '5', 'ACTIVE', null, true),
    ('screw3', 2, '6', 'ACTIVE', null, true),
    ('screw4', 2, '7', 'ACTIVE', null, true),
    ('screw5', null, '8', 'ACTIVE', null, true),
    ('screw6', null, '9', 'ACTIVE', null, true),
    ('pipe1', 1, '10', 'ACTIVE', null, false),
    ('pipe2', null, '11', 'ACTIVE', null, false);

CREATE TABLE t_components_working_hours (
id serial PRIMARY KEY,
component_id integer REFERENCES t_components(id) NOT NULL,
creation_time timestamp NOT NULL,
working_hours integer NOT NULL,
comments varchar
);

INSERT INTO t_components_working_hours (component_id, creation_time, working_hours) VALUES
    (1, current_timestamp - interval '1 day', 120),
    (3, current_timestamp - interval '2 day', 60);

CREATE TABLE t_schedule_rules(
id serial PRIMARY KEY,
name VARCHAR NOT NULL,
component_id integer REFERENCES t_components(id) NOT NULL,
rule_type VARCHAR NOT NULL,
start_date timestamp NOT NULL,
end_date timestamp,
frequency_type VARCHAR NOT NULL,
days_frequency integer,
working_hours_frequency integer,
description VARCHAR(1024)
);

INSERT INTO t_schedule_rules (name, component_id, rule_type, start_date, end_date,
            frequency_type, days_frequency, working_hours_frequency) VALUES
    ('rule1', 1, 'INSPECT', date_trunc('day',current_timestamp), null,'EACH_DAYS_INTERVAL', 2, null),
    ('rule2', 1, 'INSPECT', date_trunc('day',current_timestamp) - interval '4 day', null,'EACH_DAYS_INTERVAL', 2, null),
    ('rule3', 1, 'INSPECT', date_trunc('day',current_timestamp) - interval '3 day', null,'EACH_DAYS_INTERVAL', 2, null),
    ('rule4', 2, 'CLEAN', date_trunc('day',current_timestamp) - interval '1 day', null,'EACH_DAYS_INTERVAL', 1, null),
    ('rule5', 1, 'REPAIR', date_trunc('day',current_timestamp) - interval '2 day', null,'EACH_WORKING_HOURS_INTERVAL', null, 100),
    ('rule6', 3, 'INSPECT', date_trunc('day',current_timestamp) - interval '3 day', null,'EACH_DAYS_OR_WORKING_HOURS_INTERVAL', 4, 40),
    ('rule7', 10, 'CLEAN', date_trunc('day',current_timestamp) - interval '2 day', current_timestamp - interval '1 day', 'EACH_PORT_DOCK_STOP', null, null),
    ('rule8', 2, 'MEASURE', date_trunc('day',current_timestamp), null, 'EACH_PORT_DOCK_STOP', null, null);

CREATE TABLE t_jobs (
id serial PRIMARY KEY,
name VARCHAR NOT NULL,
type VARCHAR NOT NULL,
component_id integer REFERENCES t_components(id) NOT NULL,
schedule_rule_id integer REFERENCES t_schedule_rules(id),
scheduled_time timestamp,
completion_time timestamp,
worked_hours jsonb,
status varchar,
crew boolean,
outsourced boolean,
class_attendance boolean NOT NULL DEFAULT FALSE,
comments varchar(2048),
attachment_url varchar
);

INSERT INTO t_jobs (name, type, component_id, schedule_rule_id, scheduled_time, completion_time, status) VALUES
    ('job1', 'INSPECT', 1, 2, date_trunc('day',current_timestamp) - interval '2 day',current_timestamp - interval '25 hour', 'DONE'),
    ('job2', 'INSPECT', 1, 3, date_trunc('day',current_timestamp) - interval '2 day', null, null);

CREATE TABLE t_logs (
id serial PRIMARY KEY,
crew_member varchar NOT NULL,
log_time timestamp NOT NULL,
job_id integer REFERENCES t_jobs(id),
event_logged varchar NOT NULL,
attachment_url varchar
);

INSERT INTO t_logs (crew_member, log_time, job_id, event_logged) VALUES
    ('james', current_timestamp - interval '2 day', 1, 'DONE'),
    ('captain', current_timestamp - interval '1 day', null, 'The deck has been cleared');

CREATE TABLE t_port_arrivals (
id serial PRIMARY KEY,
destination varchar NOT NULL,
arrival_date timestamp,
departure_date timestamp
);

INSERT INTO t_port_arrivals (destination, arrival_date, departure_date) VALUES
    ('Trieste', current_timestamp - interval '3 day', current_timestamp - interval '1 day'),
    ('London',current_timestamp + interval '1 day', null),
    ('Monaco', current_timestamp + interval '3 day', current_timestamp + interval '5 day');
